﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace RafalRatajczykLab2.Controller
{
    public static class FormMainController
    {
        /// <summary>
        /// Metoda zapisuje dane z tabeli do pliku wybranego przez użytkownika
        /// </summary>
        public static void SaveToTextFile(DataGridView dataGridViewArrivals)
        {
            string textToSave = ""; // zmienna na zawartość zapisywaną do pliku 
            SaveFileDialog saveFileDialog = new SaveFileDialog(); // otwiera okno do wybrania lokalizacji do zapisu do pliku
            saveFileDialog.Filter = "txt|*.txt|inny|*.*"; // ustawia filtr na txt (domyślnie) lub inne (*.*)
            DialogResult dialogResult = saveFileDialog.ShowDialog(); // pokazanie okna dialogowego
            if (dialogResult == System.Windows.Forms.DialogResult.OK) // jeśli użytkownik podał nazwę i kliknął OK, to zapisywanie
            {
                // konstrukcja using zapewnia, że strumień zostanie automatcyznie zamknięty po wyjściu z tejże
                using (StreamWriter streamWriter = new StreamWriter(saveFileDialog.FileName))
                {
                    for (int i = 0; i < dataGridViewArrivals.Rows.Count - 1; i++)
                    {
                        textToSave = ""; // trzeba wyczyścić tekst
                        textToSave += dataGridViewArrivals.Rows[i].Cells["RegistryNumber"].Value.ToString() + "; "; // dodaje wartość danego wiersza z kolumny RegistryNumber 
                        textToSave += dataGridViewArrivals.Rows[i].Cells["Supply"].Value.ToString() + "; "; // dodaje wartość danego wiersza z kolumny Supply
                        textToSave += dataGridViewArrivals.Rows[i].Cells["Amount"].Value.ToString(); // dodaje wartość danego wiersza z kolumny Amount
                        streamWriter.WriteLine(textToSave); // zapisuje do pliku od nowej linii 
                    }
                }
                MessageBox.Show("Zapisano poprawnie do pliku :)", "Zapisano poprawnie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                System.Diagnostics.Process.Start("notepad.exe", saveFileDialog.FileName); // uruchamia zapisany plik w Notatniku
            }
        }

        /// <summary>
        /// Zapisuje dane do pliku XML
        /// </summary>
        /// <param name="dataGridViewArrivals">tabela, z której pobierane są rekordy do zapisu</param>
        public static void SaveToXml(DataGridView dataGridViewArrivals)
        {
            XDocument document = new XDocument(); // obiekt dokumentu xml, do którego będą dodawne węzły
            XElement rootElement = new XElement("AllVehicles"); // węzeł główny (root)
            document.Add(rootElement); // dodanie root'a do dokumentu 
            foreach (DataGridViewRow row in dataGridViewArrivals.Rows) // przeglądanie wszytskich wierszy tabeli
            {
                string registryNumber = (string)row.Cells["RegistryNumber"].Value; // pobieranie odpowiednich danych na podstawie nazw kolumn
                string currentSupply = (string)row.Cells["Supply"].Value;
                string amount = (string)row.Cells["Amount"].Value;
                XElement parentNode = new XElement("Vehicle"); // te wartości będą potomkami węzła Vehicle (bardziej przejrzysta struktura)
                parentNode.Add(new XElement("RegistryNumber", registryNumber), // dodanie wartości do rodzica Vehicle
                    new XElement("Supply", currentSupply), new XElement("Amount", amount));
                rootElement.Add(parentNode); // dodanie tego rodzica do elementu głównego (root)
            }
            // analogicznie jak w zapisie do pliku tekstowego (z pewnymi minimalnymi zmianami)
            SaveFileDialog saveFileDialog = new SaveFileDialog(); // otwiera okno do wybrania lokalizacji do zapisu do pliku
            saveFileDialog.Filter = "XML|*.xml|inny|*.*";         // ustawia filtr na xml (domyślnie) lub inne (*.*)
            DialogResult dialogResult = saveFileDialog.ShowDialog(); // pokazanie okna dialogowego
            if (dialogResult == System.Windows.Forms.DialogResult.OK)
            {
                document.Save(saveFileDialog.FileName); // zapis do pliku na dysku na podstawie wpisanej przez użytkownika ścieżki
            }
            MessageBox.Show("Zapisano pomyślnie do pliku XML", "Zapis do pliku pomyślny", MessageBoxButtons.OK, MessageBoxIcon.Information);
            System.Diagnostics.Process.Start("notepad.exe", saveFileDialog.FileName); // wyświetlenie pliku w Notatniku
        }

        /// <summary>
        /// Metoda losuje wybrane komórki celem dodania nowego wiersza do tabeli typu DataGridView
        /// </summary>
        /// <param name="characters">zbiór wszystkich znaków do losowania</param>
        /// <param name="supply">zbiór możliwych produktów przewożonych</param>
        /// <param name="dataGridViewArrivals">obiekt typu DataGridView, do którego będzie dodany nowy rekord</param>
        public static void GenerateNewVehicle(string characters, string[] supply, DataGridView dataGridViewArrivals)
        {
            Random randomGenerator = new Random();
            // statyczna metoda Repeat z klasy Linq.Enumerable zwróci dokładnie 7 powtórzeń znaku z łańcucha w oparciu o metodę Select 
            // metoda Select zwróci losowe znaki (wyrażenie LINQ w którym wejściowy ciąg znaków jest argumentem wyrażenia i zwraca ono
            // znak na losowej pozycji w łańcuchu w zakresie 0 - characters.Length. Metoda ToArray() sprawi, że zwracane znaki 
            // będą zapisane w tablicy typu char[], a te zamienione na łańcuch znaków w konstruktorze typu string.
            string registryNumber = new string(Enumerable.Repeat(characters, 7)
                .Select(character => character[randomGenerator.Next(characters.Length)])
                .ToArray());
            string transportedSupply = supply[randomGenerator.Next(0, supply.Length)]; // wylosuje przewożony towar z tablicy towarów 
            string amount = randomGenerator.Next(0, 10).ToString();
            dataGridViewArrivals.Rows.Add(registryNumber, transportedSupply, amount); // dodaje kolejny rekord do tabeli
        }
    }
}

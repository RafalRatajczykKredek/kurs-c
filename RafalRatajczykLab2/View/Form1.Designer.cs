﻿namespace RafalRatajczykLab2
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonNew = new System.Windows.Forms.Button();
            this.labelRegisterNumber = new System.Windows.Forms.Label();
            this.labelSupply = new System.Windows.Forms.Label();
            this.labelAmount = new System.Windows.Forms.Label();
            this.textBoxRegistryNumber = new System.Windows.Forms.TextBox();
            this.textBoxSupply = new System.Windows.Forms.TextBox();
            this.textBoxAmount = new System.Windows.Forms.TextBox();
            this.dataGridViewArrivals = new System.Windows.Forms.DataGridView();
            this.buttonSaveToFile = new System.Windows.Forms.Button();
            this.timerArrivals = new System.Windows.Forms.Timer(this.components);
            this.buttonRegistryArrivals = new System.Windows.Forms.Button();
            this.numericUpDownSimulationSpeed = new System.Windows.Forms.NumericUpDown();
            this.labelSimulationSpeed = new System.Windows.Forms.Label();
            this.radioButtonTextFile = new System.Windows.Forms.RadioButton();
            this.radioButtonXmlFile = new System.Windows.Forms.RadioButton();
            this.progressBarSimulationRunning = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArrivals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSimulationSpeed)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonNew
            // 
            this.buttonNew.Location = new System.Drawing.Point(12, 33);
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.Size = new System.Drawing.Size(97, 23);
            this.buttonNew.TabIndex = 0;
            this.buttonNew.Text = "Nowy pojazd";
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // labelRegisterNumber
            // 
            this.labelRegisterNumber.AutoSize = true;
            this.labelRegisterNumber.Location = new System.Drawing.Point(115, 17);
            this.labelRegisterNumber.Name = "labelRegisterNumber";
            this.labelRegisterNumber.Size = new System.Drawing.Size(99, 13);
            this.labelRegisterNumber.TabIndex = 1;
            this.labelRegisterNumber.Text = "Numer rejestracyjny";
            // 
            // labelSupply
            // 
            this.labelSupply.AutoSize = true;
            this.labelSupply.Location = new System.Drawing.Point(234, 17);
            this.labelSupply.Name = "labelSupply";
            this.labelSupply.Size = new System.Drawing.Size(37, 13);
            this.labelSupply.TabIndex = 2;
            this.labelSupply.Text = "Towar";
            // 
            // labelAmount
            // 
            this.labelAmount.AutoSize = true;
            this.labelAmount.Location = new System.Drawing.Point(390, 17);
            this.labelAmount.Name = "labelAmount";
            this.labelAmount.Size = new System.Drawing.Size(29, 13);
            this.labelAmount.TabIndex = 3;
            this.labelAmount.Text = "Ilość";
            // 
            // textBoxRegistryNumber
            // 
            this.textBoxRegistryNumber.Location = new System.Drawing.Point(118, 35);
            this.textBoxRegistryNumber.Name = "textBoxRegistryNumber";
            this.textBoxRegistryNumber.Size = new System.Drawing.Size(100, 20);
            this.textBoxRegistryNumber.TabIndex = 4;
            // 
            // textBoxSupply
            // 
            this.textBoxSupply.Location = new System.Drawing.Point(237, 35);
            this.textBoxSupply.Name = "textBoxSupply";
            this.textBoxSupply.Size = new System.Drawing.Size(135, 20);
            this.textBoxSupply.TabIndex = 5;
            // 
            // textBoxAmount
            // 
            this.textBoxAmount.Location = new System.Drawing.Point(393, 35);
            this.textBoxAmount.Name = "textBoxAmount";
            this.textBoxAmount.Size = new System.Drawing.Size(100, 20);
            this.textBoxAmount.TabIndex = 6;
            // 
            // dataGridViewArrivals
            // 
            this.dataGridViewArrivals.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewArrivals.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewArrivals.Cursor = System.Windows.Forms.Cursors.Default;
            this.dataGridViewArrivals.Location = new System.Drawing.Point(13, 71);
            this.dataGridViewArrivals.Name = "dataGridViewArrivals";
            this.dataGridViewArrivals.Size = new System.Drawing.Size(481, 158);
            this.dataGridViewArrivals.TabIndex = 7;
            // 
            // buttonSaveToFile
            // 
            this.buttonSaveToFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSaveToFile.Location = new System.Drawing.Point(12, 243);
            this.buttonSaveToFile.Name = "buttonSaveToFile";
            this.buttonSaveToFile.Size = new System.Drawing.Size(140, 23);
            this.buttonSaveToFile.TabIndex = 8;
            this.buttonSaveToFile.Text = "Zapisz do pliku";
            this.buttonSaveToFile.UseVisualStyleBackColor = true;
            this.buttonSaveToFile.Click += new System.EventHandler(this.buttonSaveToFile_Click);
            // 
            // buttonRegistryArrivals
            // 
            this.buttonRegistryArrivals.Location = new System.Drawing.Point(350, 243);
            this.buttonRegistryArrivals.Name = "buttonRegistryArrivals";
            this.buttonRegistryArrivals.Size = new System.Drawing.Size(143, 23);
            this.buttonRegistryArrivals.TabIndex = 9;
            this.buttonRegistryArrivals.Text = "Rejestruj przyjazdy";
            this.buttonRegistryArrivals.UseVisualStyleBackColor = true;
            this.buttonRegistryArrivals.Click += new System.EventHandler(this.buttonRegistryArrivals_Click);
            // 
            // numericUpDownSimulationSpeed
            // 
            this.numericUpDownSimulationSpeed.Location = new System.Drawing.Point(277, 244);
            this.numericUpDownSimulationSpeed.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownSimulationSpeed.Name = "numericUpDownSimulationSpeed";
            this.numericUpDownSimulationSpeed.Size = new System.Drawing.Size(55, 20);
            this.numericUpDownSimulationSpeed.TabIndex = 10;
            this.numericUpDownSimulationSpeed.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labelSimulationSpeed
            // 
            this.labelSimulationSpeed.AutoSize = true;
            this.labelSimulationSpeed.Location = new System.Drawing.Point(173, 248);
            this.labelSimulationSpeed.Name = "labelSimulationSpeed";
            this.labelSimulationSpeed.Size = new System.Drawing.Size(98, 13);
            this.labelSimulationSpeed.TabIndex = 11;
            this.labelSimulationSpeed.Text = "Szybkość symulacji";
            // 
            // radioButtonTextFile
            // 
            this.radioButtonTextFile.AutoSize = true;
            this.radioButtonTextFile.Checked = true;
            this.radioButtonTextFile.Location = new System.Drawing.Point(13, 272);
            this.radioButtonTextFile.Name = "radioButtonTextFile";
            this.radioButtonTextFile.Size = new System.Drawing.Size(87, 17);
            this.radioButtonTextFile.TabIndex = 12;
            this.radioButtonTextFile.TabStop = true;
            this.radioButtonTextFile.Text = "Plik tekstowy";
            this.radioButtonTextFile.UseVisualStyleBackColor = true;
            // 
            // radioButtonXmlFile
            // 
            this.radioButtonXmlFile.AutoSize = true;
            this.radioButtonXmlFile.Location = new System.Drawing.Point(13, 295);
            this.radioButtonXmlFile.Name = "radioButtonXmlFile";
            this.radioButtonXmlFile.Size = new System.Drawing.Size(67, 17);
            this.radioButtonXmlFile.TabIndex = 13;
            this.radioButtonXmlFile.Text = "Plik XML";
            this.radioButtonXmlFile.UseVisualStyleBackColor = true;
            // 
            // progressBarSimulationRunning
            // 
            this.progressBarSimulationRunning.Location = new System.Drawing.Point(176, 279);
            this.progressBarSimulationRunning.Name = "progressBarSimulationRunning";
            this.progressBarSimulationRunning.Size = new System.Drawing.Size(317, 23);
            this.progressBarSimulationRunning.TabIndex = 14;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(505, 314);
            this.Controls.Add(this.progressBarSimulationRunning);
            this.Controls.Add(this.radioButtonXmlFile);
            this.Controls.Add(this.radioButtonTextFile);
            this.Controls.Add(this.labelSimulationSpeed);
            this.Controls.Add(this.numericUpDownSimulationSpeed);
            this.Controls.Add(this.buttonRegistryArrivals);
            this.Controls.Add(this.buttonSaveToFile);
            this.Controls.Add(this.dataGridViewArrivals);
            this.Controls.Add(this.textBoxAmount);
            this.Controls.Add(this.textBoxSupply);
            this.Controls.Add(this.textBoxRegistryNumber);
            this.Controls.Add(this.labelAmount);
            this.Controls.Add(this.labelSupply);
            this.Controls.Add(this.labelRegisterNumber);
            this.Controls.Add(this.buttonNew);
            this.Name = "FormMain";
            this.Text = "Rejestr przyjazdów";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArrivals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSimulationSpeed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.Label labelRegisterNumber;
        private System.Windows.Forms.Label labelSupply;
        private System.Windows.Forms.Label labelAmount;
        private System.Windows.Forms.TextBox textBoxRegistryNumber;
        private System.Windows.Forms.TextBox textBoxSupply;
        private System.Windows.Forms.TextBox textBoxAmount;
        private System.Windows.Forms.DataGridView dataGridViewArrivals;
        private System.Windows.Forms.Button buttonSaveToFile;
        private System.Windows.Forms.Timer timerArrivals;
        private System.Windows.Forms.Button buttonRegistryArrivals;
        private System.Windows.Forms.NumericUpDown numericUpDownSimulationSpeed;
        private System.Windows.Forms.Label labelSimulationSpeed;
        private System.Windows.Forms.RadioButton radioButtonTextFile;
        private System.Windows.Forms.RadioButton radioButtonXmlFile;
        private System.Windows.Forms.ProgressBar progressBarSimulationRunning;
    }
}


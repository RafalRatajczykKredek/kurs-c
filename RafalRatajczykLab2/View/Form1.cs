﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml.Linq;
using RafalRatajczykLab2.Controller; // przestrzeń nazw dla statycznych klas z folderu Controller

namespace RafalRatajczykLab2
{
    public partial class FormMain : Form
    {
        string characters = "ABCDEFGHIJKLMNOPRSTUWXYZ0123456789"; // zbiór wszystkich dużych liter do losowania w celu utworzenia losowaego napisu
        string[] supply = { "Zabawki", "Warzywa", "Owoce", "Pieczywo", "Sprzęt AGD", "Art. biurowe", "Kosmetyki" }; // spis możliwych artkułów przewożonych
        Random randomGenerator = new Random(); // generator liczb losowych
        bool isRegistryOn = false; // wskazuje, czy rejestrowanie pojazdów się odbywa (true) czy jest wstrzymane (false)

        public FormMain()
        {
            InitializeComponent();
            // inicjalizacja domyślna pól tekstowych 
            this.textBoxRegistryNumber.Text = "XX173AC";
            this.textBoxSupply.Text = "Jabłka";
            this.textBoxAmount.Text = "10";
            // inicjalizacja dataGridView - dodawanie nowych kolumn
            this.dataGridViewArrivals.Columns.Add("RegistryNumber", "Numer rejestracyjny"); 
            this.dataGridViewArrivals.Columns.Add("Supply", "Towary");
            this.dataGridViewArrivals.Columns.Add("Amount", "Ilość");

            this.timerArrivals.Tick += new EventHandler(TimerArrivalsTicked); // dodanie zdarzenia na "tyknięcie" timera
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            // po naciśnięciu przycisku dodawany jest nowy wiersz
            this.dataGridViewArrivals.Rows.Add(this.textBoxRegistryNumber.Text, this.textBoxSupply.Text, this.textBoxAmount.Text);
        }

        private void buttonSaveToFile_Click(object sender, EventArgs e)
        {
            if (this.radioButtonTextFile.Checked) // jeśli zapis do pliku tekstowego został wybrany:
                FormMainController.SaveToTextFile(this.dataGridViewArrivals); // metoda w folderze Controller w statycznej klasie
            else // w przeciwnym razie wybrano zapis do XML
                FormMainController.SaveToXml(this.dataGridViewArrivals);
        }

        
        private void buttonRegistryArrivals_Click(object sender, EventArgs e)
        {
            this.timerArrivals.Interval = 1000 / (int)numericUpDownSimulationSpeed.Value; // ustawia odpowiedni interwał w zależności od ustawień użytkownika
            if (isRegistryOn == false)
            {
                this.buttonRegistryArrivals.Text = "Przerwij rejestrowanie";
                timerArrivals.Start(); // włącza timer (ponowne wciśnięcie przycisku wyłącza go)
                isRegistryOn = true;   // oznacza, że rejestrowanie pojazdów w toku 
                this.buttonSaveToFile.Enabled = false; // w trakcie działania symulacji nie można zapisywać
                this.Cursor = Cursors.WaitCursor; // ustawienie typu kursora na oczekujący (kręcoce się kółko)
                this.dataGridViewArrivals.Cursor = Cursors.WaitCursor; // po najechaniu na dataGridView niech też będzie czekający kursor
                this.progressBarSimulationRunning.MarqueeAnimationSpeed = 30; // szybkość przewijania paska progress bara
                this.progressBarSimulationRunning.Style = ProgressBarStyle.Marquee; // ustawia styl na nieskończone przewijanie się progress bara (aby pokazać, że aplikacja działa)
            }
            else
            {
                this.buttonRegistryArrivals.Text = "Rejestruj przyjazdy";
                timerArrivals.Stop(); // wstrzymuje timer aż do ponownego naciśnięcia przycisku
                isRegistryOn = false;
                this.buttonSaveToFile.Enabled = true; // po wyłączeniu timera można dokonać zapisu do pliku
                Cursor.Current = Cursors.Default; // przywrócenie typu kursora na domyślny 
                this.dataGridViewArrivals.Cursor = Cursors.Default;
                this.progressBarSimulationRunning.Style = ProgressBarStyle.Continuous; // przywrócenie domyślnego typu progress bara
            }

        }

        private void TimerArrivalsTicked(object sender, EventArgs e)
        {
            // ustawia nowy interwał losowany z przedziału (1000, 2500) wyskalowany o ustawioną szybkość
            // dla szybkości symulacji 1 losowany jest czas przybycia następnego pojazdu między 1 - 2.5 sekund
            this.timerArrivals.Interval = randomGenerator.Next(1000, 2500) / (int)numericUpDownSimulationSpeed.Value;
            FormMainController.GenerateNewVehicle(characters, supply, dataGridViewArrivals); // metoda generuje losowe wpisy do tabeli
        }

    }
}
